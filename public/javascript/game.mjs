import { showWinnerModal } from "./modal/winner.mjs";
import { 
  updateRoomInterface,
  updateProgress,
  prepareTimerStartInterface,
  drawRooms
} from "./gameInterface.mjs";

const username = sessionStorage.getItem("username");

const moveToLogin = () => window.location.replace("/login");

if (!username) {
  moveToLogin();
}

const socket = io("", { query: { username } });

socket.on("error", (error) => {
  alert(error);
  sessionStorage.clear();
  moveToLogin();
});

const root = document.getElementById("root");
const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");

socket.on("connect", () => {
  const waitingMsg = document.getElementById("waiting-msg");
  waitingMsg.classList.add("display-none");
  roomsPage.classList.remove("display-none");
});

let currentRoom = {
  username
};

const updateRoom = (room) => {
  currentRoom = {
    ...room,
    timerLeft: currentRoom?.timerLeft,
    gameLeft: currentRoom?.gameLeft,
    typed: currentRoom?.typed,
    text: currentRoom?.text,
    username: currentRoom?.username,
    gameLeftRepeater: currentRoom?.gameLeftRepeater
  };
  updateRoomInterface(currentRoom);
}

socket.on("UPDATE_ROOM", updateRoom);

const backToRooms = () => {
  socket.emit("LEAVE_ROOM");
  roomsPage.classList.remove("display-none");
  gamePage.classList.add("display-none");
}
const backToRoomsButton = document.getElementById("back-to-rooms-button");
backToRoomsButton.addEventListener("click", backToRooms);

const readyButton = document.getElementById("ready-button");
readyButton.addEventListener("click", () => socket.emit("TOGGLE_READY"));

const joinRoom = roomName => {
  socket.emit("JOIN_ROOM", { roomName }, result => {
    if(result.error) {
      alert(result.error);
      return;
    }

    roomsPage.classList.toggle("display-none");
    gamePage.classList.toggle("display-none");
    updateRoom(result);
  });
};

const createRoomButton = document.getElementById("create-room-button");
createRoomButton.addEventListener("click", () => {
  const newName = prompt("Enter the new room's name:");

  socket.emit("NEW_ROOM", newName, (result) => {
    if(result === true) {
      joinRoom(newName);
    }
    else {
      alert("Room with such name already exists");
    }
  });
})

socket.on("ROOMS_LIST", (rooms) => drawRooms(rooms, joinRoom));

socket.on("UPDATE_ROOM_USERS", (room) => {
  updateRoom(room);
});

const timerLeft = document.getElementById("timer-left")

const updateCounter = () => {
  timerLeft.innerHTML = currentRoom.timerLeft / 1000;
}

const textContainer = document.getElementById("text-container");
const gameLeftLabel = document.getElementById("game-left");


let keyBoardEventListener;
const startGame = () => {
  timerLeft.classList.add("display-none");
  textContainer.classList.remove("display-none");
  gameLeftLabel.classList.remove("display-none");
  gameLeftLabel.innerHTML = `${currentRoom.gameLeft} seconds left`;

  currentRoom.typed = 0;
  updateProgress(currentRoom);

  keyBoardEventListener = (event) => {
    const key = event.key;
    if(currentRoom.text.length === currentRoom.typed) {
      return;
    }

    if(currentRoom.text[currentRoom.typed] === key) {
      currentRoom.typed += 1;
      socket.emit("UPDATE_PROGRESS", currentRoom.typed / currentRoom.text.length);
      updateProgress(currentRoom);
    }
  };

  document.addEventListener("keydown", keyBoardEventListener);
};

const updateGameLeft = () => {
  gameLeftLabel.innerHTML = `${currentRoom.gameLeft} seconds left`;
};

const endGame = (winnerList) => {
  textContainer.classList.add("display-none");
  gameLeftLabel.classList.add("display-none");

  if(currentRoom.gameLeftRepeater) {
    clearInterval(currentRoom.gameLeftRepeater);
  }

  showWinnerModal(winnerList, () => {
    backToRoomsButton.classList.remove("display-none");
    readyButton.classList.remove("display-none");
  });
  document.removeEventListener("keydown", keyBoardEventListener);
};

socket.on("START_GAME", (gameParams) => {
  currentRoom.gameLeft = gameParams.time / 1000;
  startGame();
  currentRoom.gameLeftRepeater = setInterval(() => {
    currentRoom.gameLeft -= 1;
    if(currentRoom.gameLeft <= 0) {
      clearInterval(currentRoom.gameLeftRepeater);
      return;
    }
    updateGameLeft();
  }, 1000);
});

socket.on("START_COUNTER", (gameParams) => {
  if(!currentRoom) {
    return;
  } 

  currentRoom.timerLeft = gameParams.time;
  prepareTimerStartInterface(currentRoom);  

  fetch(`/game/texts/${gameParams.textId}`)
    .then(result => result.text())
    .then(text => {
      currentRoom.text = text;
    });

  const timerRepeater = setInterval(() => {
    currentRoom.timerLeft-=1000;
    updateCounter();

    if(currentRoom.timerLeft === 0) {
      startGame();
      clearInterval(timerRepeater);
    }
  }, 1000)
});

socket.on("END_GAME", (winnerList) => {
  endGame(winnerList);
}); 