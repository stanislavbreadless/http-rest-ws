 import { showModal } from './modal.mjs';
 import { createElement } from '../helpers/domHelper.mjs';

export function showWinnerModal(winners, onClose) {
  const winnerDescription = createElement({
    tagName: 'div',
    className: 'modal-body',
  });

  const winnerList = createElement({
    tagName: 'ol'
  });

  winners.forEach((winner) => {
    const listItem = createElement({ 
      tagName: 'li',
      className: 'winners-list-item' 
    });
    listItem.innerText = winner;
    winnerList.append(listItem);
  });

  winnerDescription.append(winnerList);

  showModal({
    title: `List of winners!`,
    bodyElement: winnerDescription,
    onClose
  })
}
