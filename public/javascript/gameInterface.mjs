import { createElement } from './helpers/domHelper.mjs';

const roomTitle = document.getElementById("room-title");

const playersList = document.getElementById("players-list");

const createPlayerInfo = (currentRoom, user) => {

  const playerInfo = createElement({ 
    tagName: "div",
    className: "player-info"
  });
  if(user.ready) {
    playerInfo.className += " ready";
  }
  if(user.name === currentRoom.username) {
    playerInfo.className += " you";
  }

  const playerName = createElement({ 
    tagName: "p",
    className: "player-name"
  });
  playerName.innerHTML = user.name;

  const playerProgressWrapper = createElement({ 
    tagName: "div",
    className: "player-progress-wrapper"
  });
  
  const playerProgress = createElement({ 
    tagName: "div",
    className: "player-progress" 
  });
  playerProgress.style.width = user.progress * 100 + "%";
  if(user.progress === 1) {
    playerProgress.className += " full";
  }
  playerProgressWrapper.append(playerProgress);

  playerInfo.append(playerName, playerProgressWrapper);
  return playerInfo;
};

const readyButton = document.getElementById("ready-button");
const backToRoomsButton = document.getElementById("back-to-rooms-button");

export const updateRoomInterface = (currentRoom) => {
  roomTitle.innerHTML = currentRoom.name;

  playersList.innerHTML = '';
  currentRoom.users.forEach(user => {
    playersList.append(createPlayerInfo(currentRoom, user));

    if(user.name === currentRoom.username) {
      readyButton.innerHTML = user.ready ? "Not Ready" :  "Ready";
    }
  });
};


const typedLetters = document.getElementById("typed-letters");
const nextLetter = document.getElementById("next-letter");
const remainingLetters = document.getElementById("remaining-letters");

export const updateProgress = (currentRoom) => {
  typedLetters.innerHTML = currentRoom.text.substring(0, currentRoom.typed);
  nextLetter.innerHTML = currentRoom.text.length === currentRoom.typed 
                         ? ''
                         : currentRoom.text[currentRoom.typed];
  
  remainingLetters.innerHTML = currentRoom.text.length > currentRoom.typed + 1
                               ? currentRoom.text.substring(currentRoom.typed + 1)
                               : '';
};

const roomsList = document.getElementById("rooms-list");
const createNewRoomCard = (roomInfo, joinRoomFunc) => {
  const roomDiv = createElement({
    tagName: "div",
    className: "room-desc"
  })

  const usersNumber = createElement({
    tagName: "p",
    className: "users-number"
  })
  usersNumber.innerHTML = `${roomInfo.userCount} user connected`;

  const roomName = createElement({
    tagName: "p",
    className: "room-name logical-block flex-centered"
  });
  roomName.innerHTML = roomInfo.name;

  const buttonWrapper = createElement({
    tagName: "div",
    className: "flex-centered full-width logical-block"
  });
  const joinButton = createElement({
    tagName: "button",
    className: "medium-button flex-centered"
  });
  joinButton.innerHTML = "Join";
  joinButton.addEventListener("click", () => joinRoomFunc(roomInfo.name));
  buttonWrapper.append(joinButton);

  roomDiv.append(usersNumber, roomName, buttonWrapper);

  roomsList.appendChild(roomDiv);
};

export const drawRooms = (rooms, joinRoomFunc) => {
  roomsList.innerHTML = '';
  rooms.forEach(room => {
    createNewRoomCard(room, joinRoomFunc);
  })
}

const timerLeft = document.getElementById("timer-left");
export const prepareTimerStartInterface = (currentRoom) => {
  backToRoomsButton.classList.add("display-none");
  readyButton.classList.add("display-none");

  timerLeft.classList.remove("display-none");
  timerLeft.innerHTML = currentRoom.timerLeft / 1000;
}