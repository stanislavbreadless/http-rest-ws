import * as config from "./config";
import useMiddleware from "./middleware";

import {
  roomExists,
  getRoomDetails,
  sendRoomsList,
  updateRoom,
  getRoomObject
} from './helpers/roomHelper';

import { 
  checkRoomForTimer,
  checkRoomForEnding
} from './helpers/gameHelper';

export default io => {
  useMiddleware(io);

  const updateRoomForSocket = (io, socket) => {
    const roomName = socket.roomName;
    if(roomExists(io, roomName)) {
      updateRoom(io, roomName);
    }
  };

  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    socket.userData = {
      name: username,
      ready: false,
    };

    sendRoomsList(io, socket);

    socket.on("disconnect", () => {

      updateRoomForSocket(io, socket);
      sendRoomsList(io, socket);
      sendRoomsList(io, socket.broadcast);
      checkRoomForTimer(io, socket.roomName);
      checkRoomForEnding(io, socket.roomName);
    });

    socket.on("NEW_ROOM", (newName, callback) => {
      if(Object.keys(io.sockets.adapter.rooms).filter(key => key === newName).length === 0) {
        callback(true);
      }
      else {
        callback(false);
      }
    });

    socket.on("JOIN_ROOM", ( { roomName }, callback ) => {

      const roomLength = getRoomObject(io, roomName)?.length;
      if(roomLength === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        callback({
          error: 'Maximum number of users in the room is reached'
        });
        return;
      } 

      socket.userData = {
        ...socket.userData,
        ready: false,
        progress: 0,
        lastPress: Date.now()
      };
      socket.roomName = roomName;

      socket.join(roomName, () => {
        getRoomObject(io, roomName).avaliable = true;
        sendRoomsList(io, socket.broadcast);

        updateRoom(io, roomName);

        callback(getRoomDetails(io, roomName));
      });
    });

    socket.on("LEAVE_ROOM", async () => {
      const roomName = socket.roomName;
      socket.leave(roomName, () => {
        if(roomExists(io, roomName)) {
          updateRoom(io, roomName);
        }
        sendRoomsList(io, socket);
        sendRoomsList(io, socket.broadcast);
        checkRoomForTimer(io, roomName);
        socket.roomName = null;
      });
    });

    socket.on("TOGGLE_READY", () => {
      socket.userData.ready = !socket.userData.ready;
      updateRoomForSocket(io, socket);
      checkRoomForTimer(io, socket.roomName);
    })

    socket.on("UPDATE_PROGRESS", ( value ) => {
      const roomObj = getRoomObject(io, socket.roomName);
      // If the game is over, return
      if(roomObj.avaliable) {
        return;
      }

      socket.userData.progress = value;
      socket.userData.lastPress = Date.now();
      updateRoom(io, socket.roomName);
      checkRoomForEnding(io, socket.roomName);
    });

  });

};
