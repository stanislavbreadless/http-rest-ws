import * as config from "../config";

export const getRoomObject = (io, roomName) => io.sockets.adapter.rooms[roomName];
export const roomExists = (io, roomName) => getRoomObject(io, roomName) ? true : false;

export const getRealRooms = (roomsObj) => Object.entries(roomsObj).filter(([key, value]) => key != value.hash);

export const getAvailableRooms = (io) => {
  return getRealRooms(io.sockets.adapter.rooms)
    .filter(([_, value]) => value.avaliable && value.length < config.MAXIMUM_USERS_FOR_ONE_ROOM)
    .map(([key, value]) => ({
      name: key,
      userCount: value.length,
      canJoin: value.length < config.MAXIMUM_USERS_FOR_ONE_ROOM
    }));
};

export const getRoomDetails = (io, roomName) => ({
  name: roomName,
  users: Object.keys(io.sockets.adapter.rooms[roomName].sockets)
               .map(socketId => io.sockets.connected[socketId].userData),
});

export const sendRoomsList = (io, socket) => socket.emit("ROOMS_LIST", getAvailableRooms(io));
export const updateRoom = (io, roomName) => io.in(roomName).emit("UPDATE_ROOM", getRoomDetails(io, roomName));

export const clearRoom = async (io, roomName) => {
  const users = getRoomDetails(io, roomName).users;
  users.forEach(user => {
    user.ready = false;
    user.progress = 0;
  });
  await io.in(roomName).emit("UPDATE_ROOM", getRoomDetails(io, roomName));
}