import { texts } from "../../data";
import {
  roomExists,
  getRoomDetails,
  sendRoomsList,
  clearRoom,
  getRoomObject
} from "./roomHelper";
import * as config from "../config";


const randomRange = (leftBound, rightBound) => {
  return Math.round(Math.random() * (rightBound - leftBound + 1) + leftBound - 0.5);
}

const getNewTextId = (old) => {
  // In case we get too unlucky and don't
  // fint the unused ID 
  const randomTryTimes = 1000;

  for(let i = 0; i < randomTryTimes; i++) {
    const newId = randomRange(0, texts.length - 1);
    if(newId !== old) {
      return newId;
    }
  }

  // In case we didn't find the new one with randomRange
  for(let i = 0; i < texts.length; i++) {
    if(i !== old) {
      return i;
    }
  }
  
  return old;
};

export const startCounter = (io, roomName) => { 
  const roomObj = getRoomObject(io, roomName);
  roomObj.oldTextId = getNewTextId(roomObj.oldTextId);
  io.in(roomName).emit("START_COUNTER", {
      time: 1000 * config.SECONDS_TIMER_BEFORE_START_GAME,
      textId: roomObj.oldTextId
  });
};

export const startGame = (io, roomName) => {
  if(!roomExists(io, roomName)) {
    return;
  }

  io.sockets.adapter.rooms[roomName].endGameTimer = setTimeout(() => {
    endGame(io, roomName);
  }, 1000 * config.SECONDS_FOR_GAME);

  io.in(roomName).emit("START_GAME", {
    time: 1000 * config.SECONDS_FOR_GAME
  });
}

export const createWinnerList = (io, roomName) => {
  const players = getRoomDetails(io, roomName).users;

  return players.sort((player1, player2) => {
    if(player1.progress === player2.progress) {
      return player1.lastPress - player2.lastPress;
    }
    return player2.progress - player1.progress;
  })
  .map(player => player.name); 
}

export const endGame = (io, roomName) => {
  if(!roomExists(io, roomName)) {
    return;
  }

  const roomObj = io.sockets.adapter.rooms[roomName];
  if(roomObj.endGameTimer) {
    clearTimeout(roomObj.endGameTimer);
  }
  roomObj.avaliable = true;
  
  
  io.in(roomName).emit("END_GAME", createWinnerList(io, roomName));
  clearRoom(io, roomName);
  sendRoomsList(io, io);
};

export const checkRoomForTimer = (io, roomName) => {
  if(!roomExists(io, roomName)) {
    return;
  }

  const users = getRoomDetails(io, roomName).users;
  if(users.some(user => !user.ready)) {
    return;
  }

  const roomObj = getRoomObject(io, roomName);
  if(!roomObj.avaliable) {
    return;
  }

  roomObj.avaliable = false;
  sendRoomsList(io, io);
  setTimeout(() => {
    startGame(io, roomName);
  }, 1000 * config.SECONDS_TIMER_BEFORE_START_GAME);

  startCounter(io, roomName);
};

export const checkRoomForEnding = (io, roomName) => {
  if(!roomExists(io, roomName)) {
    return;
  }

  const users = getRoomDetails(io, roomName).users;
  if(users.every(user => user.progress === 1)) {
    endGame(io, roomName);
  }
}