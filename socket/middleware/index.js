export default io => {
  io.use((socket, next) => {
    let username = socket.handshake.query.username;
    
    const unique = Object.values(io.sockets.sockets)
      .every(value => {
        return username != value?.userData?.name
      }); 

    if(!unique) {
      return next(new Error("This username is already taken"));
    }

    return next();
  });
};
