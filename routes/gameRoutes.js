import { Router } from "express";
import path from "path";
import data from "../data";
import { HTML_FILES_PATH } from "../config";

const router = Router();

router
  .get("/texts/:id", (req, res) => {
    const id = req.params.id;
    if(data.texts.length > id) {
      res.status(200);
      res.send(data.texts[id]);
    }
    else {
      res.status(404);
      res.send("Text with such id was not found");
    }
  })
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })

export default router;
